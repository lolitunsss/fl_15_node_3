const {Truck} = require('../models/truckModel');

const getTrucksByUserId = async (userId) => {
    const trucks = await Truck.find({userId});
    return trucks;
}

const getTruckByIdForUser = async (truckId, userId) => {
    const truck = await Truck.findOne({_id: truckId, userId});
    return truck;
}

const addTruckToUser = async (userId, truckPayload) => {
    const truck = new Truck({...truckPayload, userId});
    await truck.save();
}

const updateTruckByIdForUser = async (truckId, userId, data) => {
    await Truck.findOneAndUpdate({_id: truckId, userId}, { $set: data});
}

const updateTruckAssigned = async (truckId, userId) => {
    await Truck.findOneAndUpdate({_id: truckId, userId}, { $set:{assigned_to: userId}});
}

const deleteTruckByIdForUser = async (truckId, userId) => {
    await Truck.findOneAndRemove({_id: truckId, userId});
}

module.exports = {
    getTrucksByUserId,
    getTruckByIdForUser,
    addTruckToUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser,
    updateTruckAssigned
};