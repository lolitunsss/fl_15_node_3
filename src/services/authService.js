const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

const registration = async ({email, password, role}) => {
    const user = new User({
        email,
        password: await bcrypt.hash(password, 10),
        role,
    });
    if(role != 'DRIVER' && role != 'SHIPPER' ){
        throw new Error('Role should be "DRIVER" or "SHIPPER"');
    }
    await user.save();
}

const signIn = async ({email, password}) => {
    const user = await User.findOne({email});

    if (!user) {
        throw new Error('Invalid email or password');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new Error('Invalid email or password');
    }

    const jwt_token = jwt.sign({
        _id: user._id,
        email: user.email
    }, 'secret');
    return jwt_token;
}


const deleteUser = async (userId) => {
    await User.findOneAndRemove({_id: userId});
  };
  



const getUserByUserId = async (userId) => {
    const user = await User.find({userId});
    return user;
}


module.exports = {
    registration,
    signIn,
    deleteUser,
    getUserByUserId
};