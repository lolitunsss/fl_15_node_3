const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel');
const {Truck} = require('../models/truckModel');

const getLoadsByUserId = async (userId) => {
    const load = await Load.find({userId});
    return load;
}

const addUserLoad = async (userId, loadPayload) => {
    const load = new Load({...loadPayload, userId});
    await load.save();
}


const getUserActiveLoad = async (userId) => {
    const load = await Load.findOne({ assigned_to: userId });
    return load ;
  };


const getUserLoadById = async (loadId, userId) => {
    const load = await Load.findOne({_id: loadId, userId});
    return load;
}


const loadState = async (userId) => {
    const load = await Load.findOne({ assigned_to: userId,  state: {$ne: null},  });
    return load
  };

  
const updateLoadByIdForShipper = async (loadId, userId, data) => {
    await Load.findOneAndUpdate({_id: loadId, userId}, { $set: data});
}



const deleteLoadByIdForUser = async (loadId, userId) => {
    await Load.findOneAndRemove({_id: loadId, userId});
}





module.exports = {
    getLoadsByUserId,
    addUserLoad,
    getUserLoadById,
    updateLoadByIdForShipper,
    deleteLoadByIdForUser ,
    getUserActiveLoad,
    loadState

};