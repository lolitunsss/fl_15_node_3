const {User} = require('../models/userModel');

const getUserByUserId = async (userId) => {
    const user = await User.findById(userId);
    return {
        _id: user._id,
        role: user.role,
        email: user.email,
        created_date: user.createdAt
    };
}

  



module.exports = {
    getUserByUserId
}