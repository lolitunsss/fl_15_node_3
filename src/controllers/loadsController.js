const express = require('express');
const router = express.Router();
const {User} = require('../models/userModel');


const {
    getLoadsByUserId,
    addUserLoad,
    getUserLoadById,
    updateLoadByIdForShipper,
    deleteLoadByIdForUser ,
    getUserActiveLoad,
    loadState

 
} = require('../services/loadsService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');


router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const loads = await getLoadsByUserId(userId);

    res.json({loads});
}));

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    await addUserLoad(userId, req.body);

    res.json({message: "Load created successfully"});
}));


router.get(  '/active', asyncWrapper(async (req, res) => {
    const { _id } = req.user;
    const load = await getUserActiveLoad(_id);
    res.json({load});
}),
);




router.patch('/active/state', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const user = await User.findOne({_id: userId});
    const role =user.role;
    if( role === 'SHIPPER'){
        throw new Error('Abailable only for driver')
    } 
    const state = await loadState(userId);
    res.json({message: `Load state changed to '${state}'`});
  }));
  

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const load = await getUserLoadById(id, userId);

    if (!load) {
        throw new InvalidRequestError('No load with such id found!');
    }

    res.json({load});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;
    const user = await User.findOne({_id: userId});
    const role =user.role;
    await  updateLoadByIdForShipper(id, userId, data, role);
    if( role != 'SHIPPER'){
        throw new Error('Abailable only for shipper')
    } 
    res.json({message: "Load updated successfully"});
}));



router.delete('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const user = await User.findOne({_id: userId});
    const role =user.role;
    if( role != 'SHIPPER'){
        throw new Error('Abailable only for shipper')
    } 
    await deleteLoadByIdForUser(id, userId);
    res.json({message: "Load deleted successfully"});
}));





module.exports = {
    loadsRouter: router
}