const express = require('express');
const { asyncWrapper } = require('../utils/apiUtils');
const router = express.Router();

const {
    getUserByUserId
} = require('../services/userService');


router.get('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const user = await getUserByUserId(userId);
    res.json({user});
}));



router.delete('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    await deleteUser(userId);
    res.json({message: 'Success'});
  }));





module.exports = {
    userRouter: router
}