const express = require('express');
const router = express.Router();

const {
    registration,
    signIn
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    registrationValidator
} = require('../middlewares/validationMidlleware');

router.post('/register', registrationValidator, asyncWrapper(async (req, res) => {
    const {
        email,
        password,
        role
    } = req.body;

    await registration({email, password, role});

    res.json({message: 'Account created successfully!'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        email,
        password
    } = req.body;

    const jwt_token = await signIn({email, password});

    res.json({jwt_token, message: 'Logged in successfully!'});
}));







module.exports = {
    authRouter: router
}