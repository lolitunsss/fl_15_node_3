const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },

  createdDate: {
    type: Date,
    default: Date.now(),
  },
  status:{
    type : String,
    default:  'IS'
  } ,
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default:  null
  }
});

module.exports = {Truck};
