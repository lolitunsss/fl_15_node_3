const mongoose = require('mongoose');

const Load = mongoose.model('Load', {
  userId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    } ,
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId
    },
    status:{
      type : String,
      enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
      default:  'NEW'
    },
    state: {
      type : String,
      default:  'En route to Pick Up'
    }, 
    name:{
      type : String,
      required: true,
    }, 
    payload:{
      type : Number,
      required: true,
    }, 
    pickup_address:{
      type : String,
    }, 
    delivery_address:{
      type : String,
      required: true,
    }, 
    dimensions:{
      type : Array ,
      required: true,
      width:{
        type: Number,
        required: true,
      },
      length:{
        type: Number,
        required: true,
      },
      height:{
        type: Number,
        required: true,
      },
    }

  });


  module.exports = {Load};


