const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
    const {
        authorization
    } = req.headers;

    if (!authorization) {
        return res.status(401).json({message: 'Please, provide "authorization" header'});
    }

    const [, jwt_token] = authorization.split(' ');

    if (!jwt_token) {
        return res.status(401).json({message: 'Please, include jwt_token to request'});
    }

    try {
        const jwt_tokenPayload = jwt.verify(jwt_token, 'secret');
        req.user = {
            userId: jwt_tokenPayload._id,
            email: jwt_tokenPayload.email,
        };
        next();
    } catch (err) {
        res.status(401).json({message: err.message});
    }
}

module.exports = {
    authMiddleware
}